#!/bin/bash
echo "**** Begin installing Java and Maven"

# Install maven
export MVN_VERSION=3.6.3
sudo wget http://artfiles.org/apache.org/maven/maven-3/$MVN_VERSION/binaries/apache-maven-$MVN_VERSION-bin.tar.gz
sudo tar -xvf apache-maven-$MVN_VERSION-bin.tar.gz -C /usr/local/

# Install java
export JAVA_VERSION=11.0.2
sudo wget https://download.java.net/java/GA/jdk11/9/GPL/openjdk-${JAVA_VERSION}_linux-x64_bin.tar.gz
sudo mkdir /usr/lib/jvm
sudo tar -xvf openjdk-${JAVA_VERSION}_linux-x64_bin.tar.gz -C /usr/lib/jvm/

# Set environment
sudo echo PATH=/usr/lib/jvm/jdk-$JAVA_VERSION/bin:/usr/local/apache-maven-$MVN_VERSION/bin:$PATH > /etc/environment
sudo echo M2_HOME=/usr/local/apache-maven-$MVN_VERSION >> /etc/environment
sudo echo JAVA_HOME=/usr/lib/jvm/jdk-$JAVA_VERSION >> /etc/environment
